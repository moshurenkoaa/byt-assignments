public interface InterfaceChain {
    void setNextChain(InterfaceChain nextChain);

    void calculate(Operation op);
}
