public class Multiplication implements InterfaceChain {
    private InterfaceChain chain;
    @Override
    public void setNextChain(InterfaceChain nextChain) {
        this.chain=nextChain;
    }

    @Override
    public void calculate(Operation op) {
        if(op.getSymbol().equals("*")){
            System.out.println("Result: "+ (op.getFirst()*op.getSecond()));
        }
        else {
            this.chain.calculate(op);
        }
    }
}