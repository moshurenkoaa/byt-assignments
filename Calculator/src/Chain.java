import java.util.Scanner;
public class Chain {

        private InterfaceChain c1;

        public Chain() {

            this.c1 = new Addition();
            InterfaceChain c2 = new Division();
            InterfaceChain c3 = new Subtraction();
            InterfaceChain c4 = new Multiplication();

            // set the chain of responsibility
            c1.setNextChain(c2);
            c2.setNextChain(c3);
            c3.setNextChain(c4);
        }

        public static void main(String[] args) {
            Chain calcChain = new Chain();
            while (true) {
                int first;
                int second;
                String op;
                System.out.println("Please provide first number: ");
                Scanner input = new Scanner(System.in);
                first = input.nextInt();
                System.out.println("Please provide the operand: ");
                input = new Scanner(System.in);
                op = input.nextLine();
                System.out.println("Please provide second number: ");
                input = new Scanner(System.in);
                second = input.nextInt();
                Operation oper = new Operation(first,second,op);
                calcChain.c1.calculate(oper);
            }

        }

    }

