public class Operation {
    private int first;
    private String symbol;
    private int second;

    public Operation(int first, int second , String op){
        this.first=first;
        this.second=second;
        this.symbol=op;

    }
    public int getFirst(){
        return first;
    }

    public int getSecond(){
        return second;
    }
    public String getSymbol(){
        return symbol;
    }
}
