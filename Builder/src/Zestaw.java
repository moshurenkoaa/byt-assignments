public abstract class Zestaw {
    abstract void pickMain();
    abstract void pickDrink();
    abstract void pickFries();
    public final void process(){
        pickMain();
        pickDrink();
        pickFries();

    }
}
