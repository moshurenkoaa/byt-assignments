public class BigMac extends Zestaw {
    @Override
    void pickMain() {
        System.out.println("BigMac burger has been selected as a Main dish");
    }

    @Override
    void pickDrink() {
        System.out.println("Pepsi 0.5l has been selected as a Drink");
    }

    @Override
    void pickFries() {
        System.out.println("Custom LA edition fries has been selected");
    }
}