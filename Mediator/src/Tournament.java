import java.util.ArrayList;
import java.util.List;

public class Tournament {
    public static void main(String[] args) {
        int current = 0;
        int winner = 0;
        Originator originator = new Originator();
        CareTaker careTaker = new CareTaker();

        Participant first = new Participant("Andrew");
        Participant second = new Participant("Michael");
        Participant third = new Participant("Bryan");
        Participant n = new Participant("Bob");

        List<Participant> partList = new ArrayList<Participant>();
        first.setPoints(15);
        second.setPoints(20);
        n.setPoints(35);
        third.setPoints(30);
        partList.add(first);
        partList.add(second);
        partList.add(third);
        partList.add(n);
        for (int i = 0; i < partList.size(); i++) {
            current = partList.get(i).getPoints();
            if (current > winner) {
                winner = current;
                originator.setState(partList.get(i).getName());
                careTaker.add(originator.rememberMe());
            }
        }
        System.out.println("Winner: " + originator.getState());

        for (int i = 0; i < careTaker.sizeL() - 1; i++) {
            originator.remindMe(careTaker.get(i));
            System.out.println("Another candidates for a prize: " + originator.getState());

        }
    }
}
