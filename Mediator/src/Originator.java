public class Originator {
    private String state;

    public void setState(String state){
        this.state=state;

    }

    public String getState(){
        return state;

    }

    public Memento rememberMe(){
        return new Memento(state);
    }
    public void remindMe(Memento memory) {
        state=memory.getState();
    }
}
