
/* "ConcreteBuilder" */
class FlatBuilder extends HouseBuilder {
    public void buildDoor() {
        home.setDoor("Metal door");
    }

    public void buildWindows() {
        home.setWindows("2 middle-sized windows");
    }

    public void buildInterior() {
        home.setInterior("Casual low-budget interior");
    }
}
