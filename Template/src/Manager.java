/* "Director" */
class Manager {
    private HouseBuilder houseBuilder;

    public void setHouseBuilder(HouseBuilder hb) {
        houseBuilder = hb;
    }

    public Home getHome() {
        return houseBuilder.getHouse();
    }

    public void constructHouse() {
        houseBuilder.createNewHouse();
        houseBuilder.buildDoor();
        houseBuilder.buildWindows();
        houseBuilder.buildInterior();
    }
}