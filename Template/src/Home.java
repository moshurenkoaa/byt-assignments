/* "Product" */
class Home {
    private String door = "";
    private String windows = "";
    private String Interior = "";

    public void setDoor(String door) {
        this.door = door;
    }

    public void setWindows(String windows) {
        this.windows = windows;
    }

    public void setInterior(String interior) {
        this.Interior = interior;
    }
    public String getDoor(){
        return door;
    }
    public String getWindows(){
        return windows;
    }
    public String getInterior(){
        return Interior;
    }
}

