
/* "ConcreteBuilder" */
class CottageBuilder extends HouseBuilder {
    public void buildDoor() {
        home.setDoor("2 Wooden doors from both sides");
    }

    public void buildWindows() {
        home.setWindows("3 panorama windows");
    }

    public void buildInterior() {
        home.setInterior("High-budget wooden interior");
    }
}
