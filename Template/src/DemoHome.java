/* A customer ordering a pizza. */
public class DemoHome {
    public static void main(String[] args) {
        Manager manager = new Manager();
        FlatBuilder flat = new FlatBuilder();
        CottageBuilder cottage = new CottageBuilder();

        manager.setHouseBuilder(cottage);
        manager.constructHouse();

        Home home = manager.getHome();
        System.out.println("Doors type: " + home.getDoor());
        System.out.println("Windows type: " + home.getWindows());
        System.out.println("Interior type: " + home.getInterior());
    }
}