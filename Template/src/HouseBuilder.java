abstract class HouseBuilder {
    protected Home home;

    public Home getHouse() {
        return home;
    }

    public void createNewHouse() {
        home = new Home();
    }

    public abstract void buildDoor();
    public abstract void buildWindows();
    public abstract void buildInterior();
}